import java.io.*;
import java.net.ServerSocket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Server implements Runnable {

    private static long value = 0;
    private final Set<Integer> subscribers;
    private final Map<Integer, ClientHandler> clientHandlerMap;
    private final ServerSocket serverSocket;
    private static int clientCount = 0;

    public Server(ServerSocket serverSocket) {
        this(serverSocket, new HashSet<>(), new HashMap<>());
    }

    public Server(ServerSocket serverSocket, Set<Integer> subscribers, Map<Integer, ClientHandler> clientHandlerMap) {
        this.serverSocket = serverSocket;
        this.subscribers = subscribers;
        this.clientHandlerMap = clientHandlerMap;
    }

    @Override
    public void run() {
        try {
            while (true) {
                ClientHandler clientHandler = new ClientHandler(clientCount, serverSocket.accept(), this);
                clientHandlerMap.put(clientCount, clientHandler);
                clientHandler.start();
                clientCount++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void notifySubscribers() {
        subscribers.forEach(id -> {
            try {
                System.out.printf("Notifying subscriber, client '%d'...%n", id);
                clientHandlerMap.get(id).sendResponse(new Response(value));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void addSubscriber(int id) {
        subscribers.add(id);
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        Server.value = value;
    }

    public static void main(String[] args) throws IOException {
        new Server(new ServerSocket(5555)).run();
    }
}
