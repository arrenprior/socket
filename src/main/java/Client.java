import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client implements Runnable {

    Socket socket;
    BufferedWriter out;
    BufferedReader in;

    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    public void run() {
        try {
            socket = new Socket("localhost", 5555);
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            Scanner scanner = new Scanner(System.in);
            while (scanner.hasNextLine()) {
                Request request;
                try {
                    request = mapper.readValue(scanner.nextLine(), Request.class);
                } catch (JsonProcessingException e) {
                    System.out.println(e.getMessage());
                    continue;
                }

                switch (request.getCommand()) {
                    case GET:
                        sendRequest(request);
                        System.out.println(in.readLine());
                        break;
                    case SET:
                        sendRequest(request);
                        break;
                    case SUBSCRIBE:
                        sendRequest(request);

                        String response;
                        while ((response = in.readLine()) != null) {
                            System.out.println(response);
                        }
                        break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendRequest(Request request) throws IOException {
        out.write(mapper.writeValueAsString(request));
        out.newLine();
        out.flush();
    }

    public static void main(String[] args) {
        new Client().run();
    }
}
