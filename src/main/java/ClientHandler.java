import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.Socket;

public class ClientHandler extends Thread {

    private static final ObjectMapper mapper = new ObjectMapper();

    private final int id;
    private final Server server;
    private final BufferedReader in;
    private final BufferedWriter out;

    public ClientHandler(int id, Socket socket, Server server) throws IOException {
        this.id = id;
        this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        this.server = server;
    }

    public void run() {
        try {
            System.out.printf("Handling new client '%d'...%n", id);
            String requestJson;
            while ((requestJson = in.readLine()) != null) {
                handleRequest(mapper.readValue(requestJson, Request.class));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handleRequest(Request request) throws IOException {
        switch(request.getCommand()) {
            case GET:
                System.out.printf("Handling 'get' request for client '%d'...%n", id);
                long value = server.getValue();
                sendResponse(new Response(value));
                break;
            case SET:
                System.out.printf("Handling 'set' request for client '%d'...%n", id);
                server.setValue(request.getValue());
                server.notifySubscribers();
                break;
            case SUBSCRIBE:
                System.out.printf("Adding subscriber, client '%d'...%n", id);
                server.addSubscriber(id);
                break;
        }
    }

    public void sendResponse(Response response) throws IOException {
        out.write(mapper.writeValueAsString(response));
        out.newLine();
        out.flush();
    }
}