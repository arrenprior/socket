import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Request {

    enum Command {
        @JsonProperty("get")
        GET,
        @JsonProperty("set")
        SET,
        @JsonProperty("subscribe")
        SUBSCRIBE
    }

    private Command command;
    private long value;

    public Request() {}

    @JsonCreator
    public Request(@JsonProperty(value = "command", required = true) Command command, @JsonProperty(value = "value") long value) {
        this.command = command;
        this.value = value;
    }

    public Request(Command command) {
        this.command = command;
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }
}
