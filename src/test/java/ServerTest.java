import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.mockito.Mockito.*;

public class ServerTest {

    @Test
    public void testNotifySubscribers() throws IOException {
        // Arrange
        int id42 = 42;
        int id1337 = 1337;
        ClientHandler mockClientHandler42 = mock(ClientHandler.class);
        ClientHandler mockClientHandler1337 = mock(ClientHandler.class);
        Map<Integer, ClientHandler> clientHandlerMap = Map.of(id42, mockClientHandler42, id1337, mockClientHandler1337);
        Set<Integer> subscribers = Set.of(id42);
        Server server = new Server(null, subscribers, clientHandlerMap);
        server.setValue(666);
        ArgumentCaptor<Response> captor = ArgumentCaptor.forClass(Response.class);

        // Act
        server.notifySubscribers();

        // Assert
        verify(mockClientHandler42, times(1)).sendResponse(captor.capture());
        assert captor.getValue().getValue() == 666L;
        verify(mockClientHandler1337, never()).sendResponse(any());
    }

    @Test
    public void testAddSubscriber() {
        // Arrange
        Set<Integer> mockSubscribers = mock(Set.class);
        Server server = new Server(null, mockSubscribers, new HashMap<>());

        // Act
        server.addSubscriber(1337);

        // Assert
        verify(mockSubscribers, times(1)).add(1337);
    }
}
