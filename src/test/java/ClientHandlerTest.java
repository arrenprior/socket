import org.junit.jupiter.api.Test;

import java.io.*;
import java.net.Socket;

import static org.mockito.Mockito.*;

public class ClientHandlerTest {

    private final int id = 42;
    private final Socket mockSocket = mock(Socket.class);
    private final Server mockServer = mock(Server.class);

    @Test
    public void testGet() throws IOException {
        // Arrange
        String getRequest = "{\"command\": \"get\"}";
        InputStream inputStream = new ByteArrayInputStream(getRequest.getBytes());
        when(mockSocket.getInputStream()).thenReturn(inputStream);
        OutputStream outputStream = new ByteArrayOutputStream();
        when(mockSocket.getOutputStream()).thenReturn(outputStream);
        ClientHandler clientHandler = new ClientHandler(id, mockSocket, mockServer);
        when(mockServer.getValue()).thenReturn(1337L);

        // Act
        clientHandler.run();

        // Assert
        String expectedResponse = "{\"value\":1337}\n";
        String response = outputStream.toString();

        assert response.equals(expectedResponse);
    }

    @Test
    public void testSet() throws IOException {
        // Arrange
        String request = "{\"command\": \"set\", \"value\": 1337}";
        InputStream inputStream = new ByteArrayInputStream(request.getBytes());
        when(mockSocket.getInputStream()).thenReturn(inputStream);
        OutputStream outputStream = new ByteArrayOutputStream();
        when(mockSocket.getOutputStream()).thenReturn(outputStream);
        ClientHandler clientHandler = new ClientHandler(id, mockSocket, mockServer);

        // Act
        clientHandler.run();

        // Assert
        verify(mockServer, times(1)).setValue(1337);
    }

    @Test
    public void testSubscribe() throws IOException {
        // Arrange
        String request = "{\"command\": \"subscribe\"}";
        InputStream inputStream = new ByteArrayInputStream(request.getBytes());
        when(mockSocket.getInputStream()).thenReturn(inputStream);
        OutputStream outputStream = new ByteArrayOutputStream();
        when(mockSocket.getOutputStream()).thenReturn(outputStream);
        ClientHandler clientHandler = new ClientHandler(id, mockSocket, mockServer);

        // Act
        clientHandler.run();

        // Assert
        verify(mockServer, times(1)).addSubscriber(id);
    }

    @Test
    public void testSendResponse() throws IOException {
        // Arrange
        InputStream inputStream = new ByteArrayInputStream("".getBytes());
        when(mockSocket.getInputStream()).thenReturn(inputStream);
        OutputStream outputStream = new ByteArrayOutputStream();
        when(mockSocket.getOutputStream()).thenReturn(outputStream);
        ClientHandler clientHandler = new ClientHandler(id, mockSocket, mockServer);

        // Act
        clientHandler.sendResponse(new Response(1337));

        // Assert
        String expectedResponse = "{\"value\":1337}\n";
        String response = outputStream.toString();

        assert response.equals(expectedResponse);
    }
}
