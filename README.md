# Instructions

This document provides instructions to build, test and run this project.

# Building & Testing

Maven is used to build and test this project. In order to build and test run the following command from the root 
directory:

```
mvn clean install
```

# Running

It is possible to run the Server and Client applications using an IDE (I am using IntelliJ). Set up configurations to
run the main methods of the Server and Client classes.

Alternatively, the server can be run from a terminal windows using the following command:

```
mvn exec:java -Dexec.mainClass="Server" 
```

The client can similarly be run using the following command:

```
mvn exec:java -Dexec.mainClass="Client" 
```

